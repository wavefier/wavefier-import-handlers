import os
from unittest import TestCase

from wavefier_common.RawData import RawData
from wavefier_common.util.Path import Path
from wavefier_import_handlers.RawListener import RawListener


class MyListener(RawListener):

    def __init__(self, owner, excepts) -> None:
        super().__init__()
        self.owner = owner
        self.excepts = excepts

    def update(self, raw: RawData):
        self.owner.assertEqual(self.excepts, raw)


class TestRawListener(TestCase):

    def test_createTriggerListener(self):
        self.assertIsNotNone(RawListener())

    def test_update(self):
        script_dir = os.path.abspath(os.path.dirname(__file__))
        subdir = os.path.join(script_dir, 'test_static_data')

        file_path = str(Path(subdir)/'V-aaa-1222593214.gwf')
        f = open(file_path, "rb")
        print(file_path)
        excepted = RawData("V-aaa-1222593214.gwf", f.read())
        i = MyListener(self, excepted)
        i.update(excepted)



