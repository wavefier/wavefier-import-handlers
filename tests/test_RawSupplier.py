import os
from unittest import TestCase

from wavefier_common.RawData import RawData
from wavefier_common.kafka.Producer import KafkaProducer
from wavefier_common.util.Observable import Observable
from wavefier_common.util.Path import Path

from wavefier_import_handlers.RawSupplier import RawSupplier


class MyListener(Observable):

    def __init__(self, owner, sup):
        self.owner = owner
        self.sup = sup

    def update(self, context, msg_consumed):
        self.owner.assertEqual(True, True)
        self.sup.stopObserving()


class RawSupplierTest(TestCase):

    def test_creation_object(self):
        broker = "kafka_test:9092"

        TestUtil.sendMessage()

        sup = RawSupplier(broker)
        sup.addRawListener(MyListener(self, sup))
        sup.startObserving()

        sup.join()


class TestUtil:
    @staticmethod
    def sendMessage():
        broker = "kafka_test:9092"
        script_dir = os.path.abspath(os.path.dirname(__file__))
        subdir = os.path.join(script_dir, 'test_static_data')

        file_path = str(Path(subdir)/'V-aaa-1222593214.gwf')
        f = open(file_path, "rb")
        message = RawData("V-aaa-1222593214.gwf", f.read())

        producer = KafkaProducer({'bootstrap.servers': broker})
        producer.publish_message(message)
