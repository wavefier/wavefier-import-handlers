[![pipeline status](https://gitlab.trust-itservices.com/wavefier/wavefier-sv/badges/master/pipeline.svg)](https://gitlab.trust-itservices.com/wavefier/wavefier-sv/commits/master)
[![coverage report](https://gitlab.trust-itservices.com/wavefier/wavefier-sv/badges/master/coverage.svg)](https://gitlab.trust-itservices.com/wavefier/wavefier-sv/commits/master)

# Import handler Library
This library has the intent to contain the way to retrieve the files on kafka

## Usefull commands


### Create Source dist build
To create a source distribution, you run: `python setup.py sdist`. It create a tar gz on a directory dist with a source distribution

### Create Binary dist build
To create a binary distribution called a wheel, you run: `python setup.py bdist`

Other opions can be used:
* `python setup.py bdist --formats=rpm` rpm Format
* `python setup.py bdist --formats=wininst` window installer 

## Development 
Create the docker immage for the develop with the command  ` docker build -t wth-env .` Run the develop bash with `docker run -it -v $(pwd):/app wth-env /bin/bash`

### Run test
To run the test just run  `python setup.py test` 

### Run Documentation
To create documentation use the `make clean && make docs`. On the directory docs you can see the generated documentation