Welcome to wavefier-import-handlers's documentation!
========================================

The wavefier_import_handler library is part of the Wavefier project. This module contains all the code to send and receive the Raw Data in the wavefier system.


Table of content
=================

.. toctree::
   :maxdepth: 2

   structure/introduction
   structure/how_to_use_it
   structure/how_to_develop_it
   structure/code

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
