************
Installation
************


Via Python Package
==================

Install the package (or add it to your ``requirements.txt`` file):

.. code:: bash

    pip install git+https://ro:rorororo@gitlab.trust-itservices.com/wavefier/wavefier-sv.git@v0.0.1

In your ``conf.py`` file:

.. code:: python

    setup(
        ...

        install_requires=[
            ...,
            'wavefier-import-handlers',
            ...
        ],

        ...

        dependency_links=[
            'git+https://ro:rorororo@gitlab.trust-itservices.com/wavefier/wavefier-sv.git@v0.0.1#egg=wavefier-import-handlers'
        ]

        ...
    )



Via Git or Download
===================

Clone the repository via canonical command line

.. code:: bash

    git clone https://ro:rorororo@gitlab.trust-itservices.com/wavefier/wavefier-sv.git

Switch on prefered branch one has to run ``setup.py`` script from the main directory of the library.

As example you can run ``python setup.py install``