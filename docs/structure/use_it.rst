
***********************
How to use this modules
***********************



This module can be used in two different ways: as a foo library to include in your project, or as a script to launch in the shell like Command line

Library Interface
=================

The library provides different utilities, but mainly provides the mechanism to send and receive triggers from the wavefier system


RawSupplier
---------------

The class name is :class:`RawSupplier.RawSupplier` allows you to receive raw data from your wavefier system. Below a example of it


 .. code:: python

    class MyExampleOfListener(RawListener):

        def update(self, rawdata: RawData):
            print(rawdata)


    if __name__ == '__main__':

        broker_host = "kafka.mylan.lan"

        broker_port = 9092

        channel = "unit_test01"

        iMyListener = MyExampleOfListener()

        raw_supplier = RawSupplier(broker_host, broker_port)

        raw_supplier.addRawListener(iMyListener)

        raw_supplier.startObserving(channel)

