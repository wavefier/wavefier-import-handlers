#!/bin/bash

KHOST=$1
TOPIC_NAME=$2
KAFKA_WP=/opt/kafka_2.11-0.10.2.2

CMD="kafka-console-consumer.sh --bootstrap-server ${KHOST}:9092 --topic ${TOPIC_NAME} --from-beginning"

${KAFKA_WP}/bin/${CMD}
