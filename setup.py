import wavefier_import_handlers
from setuptools import setup, find_packages

setup(
    name='wavefier_import_handlers',

    packages=find_packages(
        exclude=[
            'build',
            'docs',
            'tests',
            'tools',
        ]
    ),
    url='',
    license='',
    author=wavefier_import_handlers.__author__,
    version=wavefier_import_handlers.__version__,
    description='Library to retrive astronomics files from kafka',
    long_description=open('README.md').read(),
    test_suite='nose.collector',
    setup_requires=['nose>=1.0'],
    install_requires=[ 
        'wavefier-common==2.0.0'
    ],
    dependency_links=[
        "git+https://gitlab+deploy-token-661042:fb4XV93yiAjJXsT3K3f2@gitlab.com/wavefier/wavefier-common.git#egg=wavefier-common-2.0.0"
    ]
)