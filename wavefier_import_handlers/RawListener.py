from wavefier_common import RawData


class RawListener(object):
    """
    Class used to manage the RawData coming from the system.
    """

    def update(self, context, rawdata: RawData):
        """
        Executed whenever a rawdata is present
        :param rawdata: representation of file coming from Kafka
        """
        pass
