import logging
import threading
import os
import random
import string

from wavefier_common.RawData import RawData
from wavefier_common.kafka.Consumer import KafkaConsumer
from wavefier_common.util.Observable import Observable
from wavefier_import_handlers import RawListener


class RawSupplier(threading.Thread):

    def __init__(self, broker: str, kafka_consumer_group_id=None, logger=None):
        """
        Core class used to Create the patters os server observer with Kafka Topics
        :param broker:
        """
        super(RawSupplier, self).__init__(name="Raw_listener_worker")

        self.logger = logger or logging.getLogger(__name__)

        self.__isStarted = False
        
        if kafka_consumer_group_id is None:
            letters = string.ascii_lowercase
            kafka_consumer_group_id = 'RawObserverGroup'.join(random.choice(letters) for i in range(10))
        else:
            kafka_consumer_group_id = 'RawObserverGroup_' + kafka_consumer_group_id


        self.__consumer = KafkaConsumer({
            'bootstrap.servers': broker,
            'group.id': kafka_consumer_group_id,
            'default.topic.config': {
                'auto.offset.reset': 'smallest'
            }
        })
        self._observable = Observable()

    def run(self):
        while self.__isStarted:
            try:
                msg_consumed = self.__consumer.consume_message()
                self._observable.setChanged()
                self._observable.notifyObservers(msg_consumed)

            except Exception as ex:
                self.logger.exception("Problem on Kafka consume Message: ",ex)

    def startObserving(self):
        """
        Start to Obeserving rawdata in a specific channel
        :param channel:
        :return:
        """
        logging.debug('Starting the observing')
        #todo: make template
        filter_template = RawData("a-a-122222222.gwd", None)
        self.__consumer.subscribe(filter_template)

        self.__isStarted = True
        self.start()

    def stopObserving(self):
        """
        Stop Observing Kafka
        :return:
        """
        logging.debug('Stop the observing')

        self.__isStarted = False
        self.join()

    def addRawListener(self, listener: RawListener):
        """
        Add new Raw Listener on Supplier
        :param listener:
        :return:
        """
        logging.debug('New Raw Listener Added')

        self._observable.addObserver(listener)

    def delRawListener(self, listener: RawListener):
        """
        Remove the Raw Listener from the Supplier
        :param listener:
        :return:
        """
        logging.debug('New Raw Listener deleted')

        self._observable.deleteObserver(listener)

