FROM wavefier/python:3.6.8-jessie


WORKDIR /app

COPY .  /app

RUN python setup.py develop
